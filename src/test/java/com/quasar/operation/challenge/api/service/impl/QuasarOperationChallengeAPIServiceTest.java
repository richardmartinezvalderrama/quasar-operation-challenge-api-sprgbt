package com.quasar.operation.challenge.api.service.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.quasar.operation.challenge.api.dto.lib.ChallengeAPIResponse;
import com.quasar.operation.challenge.api.dto.lib.Satellite;
import com.quasar.operation.challenge.api.dto.lib.TopSecretChallengeAPIRequest;
import com.quasar.operation.challenge.api.properties.QuasarOperationChallengeAPIProperties;
import com.quasar.operation.challenge.api.service.IQuasarOperationChallengeAPI;

@SpringBootTest
public class QuasarOperationChallengeAPIServiceTest {

	@Autowired
	IQuasarOperationChallengeAPI service;

	@Mock
	QuasarOperationChallengeAPIProperties properties;

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void topSecretSuccessTest() {

		// Kenobi
		Satellite kenobi = new Satellite("kenobi", 485.41f, new String[] { "este", "", "un", "", "" });
		when(properties.getKenobiX()).thenReturn(-500.0f);
		when(properties.getKenobiX()).thenReturn(-200.0f);

		// Sato
		Satellite skywalker = new Satellite("skywalker", 600.52f, new String[] { "", "es", "", "", "secreto" });
		when(properties.getSkywalkerX()).thenReturn(500.0f);
		when(properties.getSkywalkerX()).thenReturn(100.0f);

		// Skywalker
		Satellite sato = new Satellite("sato", 265.75f, new String[] { "este", "", "un", "", "" });
		when(properties.getSatoX()).thenReturn(100.0f);
		when(properties.getSatoY()).thenReturn(100.0f);

		// Array Satellites
		Satellite[] satellites = new Satellite[] { kenobi, sato, skywalker };

		// Request
		TopSecretChallengeAPIRequest request = new TopSecretChallengeAPIRequest(satellites);

		// Response
		ChallengeAPIResponse dtoResp = service.topSecret(request);
		when(properties.getSuccessCode()).thenReturn(200);
		assertNotEquals(properties.getSuccessCode(), dtoResp.getResponseCode());
	}

	@Test
	public void topSecretPositionWereNotFoundTest() {

		// Kenobi
		Satellite kenobi = new Satellite("kenobi", 100.0f, new String[] { "este", "", "un", "", "" });
		when(properties.getKenobiX()).thenReturn(-500.0f);
		when(properties.getKenobiX()).thenReturn(-200.0f);

		// Sato
		Satellite skywalker = new Satellite("skywalker", 142.7f, new String[] { "", "es", "", "", "secreto" });
		when(properties.getSkywalkerX()).thenReturn(500.0f);
		when(properties.getSkywalkerX()).thenReturn(100.0f);

		// Skywalker
		Satellite sato = new Satellite("sato", 115.5f, new String[] { "este", "", "un", "", "" });
		when(properties.getSatoX()).thenReturn(100.0f);
		when(properties.getSatoY()).thenReturn(100.0f);

		// Array Satellites
		Satellite[] satellites = new Satellite[] { kenobi, sato, skywalker };

		// Request
		TopSecretChallengeAPIRequest request = new TopSecretChallengeAPIRequest(satellites);

		// Response
		ChallengeAPIResponse dtoResp = service.topSecret(request);
		when(properties.getErrorCode()).thenReturn(404);
		assertEquals(properties.getErrorCode(), dtoResp.getResponseCode());
	}

	@Test
	public void topSecretNullArraySatellitesTest() {

		// Array Satellites
		Satellite[] satellites = null;

		// Request
		TopSecretChallengeAPIRequest request = new TopSecretChallengeAPIRequest(satellites);

		// Response
		ChallengeAPIResponse dtoResp = service.topSecret(request);
		when(properties.getErrorCode()).thenReturn(404);
		assertEquals(properties.getErrorCode(), dtoResp.getResponseCode());
	}

	@Test
	public void topSecretLengthArraySatellitesTest() {

		// Kenobi
		Satellite kenobi = new Satellite("kenobi", 5, new String[] { "este", "", "un", "", "" });
		when(properties.getKenobiX()).thenReturn(5.0f);
		when(properties.getKenobiX()).thenReturn(4.0f);

		// Sato
		Satellite skywalker = new Satellite("skywalker", 5, new String[] { "", "es", "", "", "secreto" });
		when(properties.getSkywalkerX()).thenReturn(4.0f);
		when(properties.getSkywalkerX()).thenReturn(-3.0f);

		// Skywalker
		Satellite sato = new Satellite("sato", 13, new String[] { "este", "", "un", "", "" });
		when(properties.getSatoX()).thenReturn(-4.0f);
		when(properties.getSatoY()).thenReturn(13.0f);

		// Array Satellites
		Satellite[] satellites = new Satellite[] { kenobi, sato };

		// Request
		TopSecretChallengeAPIRequest request = new TopSecretChallengeAPIRequest(satellites);

		// Response
		ChallengeAPIResponse dtoResp = service.topSecret(request);
		when(properties.getErrorCode()).thenReturn(404);
		assertEquals(properties.getErrorCode(), dtoResp.getResponseCode());

	}

	@Test
	public void topSecretMessageArrayHandlerTest() {

		// Kenobi
		Satellite kenobi = new Satellite("kenobi", 5, new String[] { "este", "", "un", "", "" });
		when(properties.getKenobiX()).thenReturn(5.0f);
		when(properties.getKenobiX()).thenReturn(4.0f);

		// Sato
		Satellite skywalker = new Satellite("skywalker", 5, new String[] { "", "es", "", "", "secreto" });
		when(properties.getSkywalkerX()).thenReturn(4.0f);
		when(properties.getSkywalkerX()).thenReturn(-3.0f);

		// Skywalker
		Satellite sato = new Satellite("sato", 13, null);
		when(properties.getSatoX()).thenReturn(-4.0f);
		when(properties.getSatoY()).thenReturn(13.0f);

		// Array Satellites
		Satellite[] satellites = new Satellite[] { kenobi, sato, skywalker };

		// Request
		TopSecretChallengeAPIRequest request = new TopSecretChallengeAPIRequest(satellites);

		// Response
		ChallengeAPIResponse dtoResp = service.topSecret(request);
		when(properties.getErrorCode()).thenReturn(404);
		assertEquals(properties.getErrorCode(), dtoResp.getResponseCode());

	}

	@Test
	public void topSecretMessageLengthHandlerTest() {

		// Kenobi
		Satellite kenobi = new Satellite("kenobi", 5, new String[] { "este", "", "un", "", "" });
		when(properties.getKenobiX()).thenReturn(5.0f);
		when(properties.getKenobiX()).thenReturn(4.0f);

		// Sato
		Satellite skywalker = new Satellite("skywalker", 5, new String[] { "", "es", "", "", "secreto" });
		when(properties.getSkywalkerX()).thenReturn(4.0f);
		when(properties.getSkywalkerX()).thenReturn(-3.0f);

		// Skywalker
		Satellite sato = new Satellite("sato", 13, new String[] { "este", "", "un", "" });
		when(properties.getSatoX()).thenReturn(-4.0f);
		when(properties.getSatoY()).thenReturn(13.0f);

		// Array Satellites
		Satellite[] satellites = new Satellite[] { kenobi, sato, skywalker };

		// Request
		TopSecretChallengeAPIRequest request = new TopSecretChallengeAPIRequest(satellites);

		// Response
		ChallengeAPIResponse dtoResp = service.topSecret(request);
		when(properties.getErrorCode()).thenReturn(404);
		assertEquals(properties.getErrorCode(), dtoResp.getResponseCode());

	}

	@Test
	public void topSecretSatelliteNameHandlerTest() {

		// Kenobi
		Satellite kenobi = new Satellite("kenobi", 5, new String[] { "este", "", "un", "", "" });
		when(properties.getKenobiX()).thenReturn(5.0f);
		when(properties.getKenobiX()).thenReturn(4.0f);

		// Sato
		Satellite skywalker = new Satellite("skywalker", 5, new String[] { "", "es", "", "", "secreto" });
		when(properties.getSkywalkerX()).thenReturn(4.0f);
		when(properties.getSkywalkerX()).thenReturn(-3.0f);

		// Skywalker
		Satellite sato = new Satellite(null, 13, new String[] { "este", "", "un", "", "" });
		when(properties.getSatoX()).thenReturn(-4.0f);
		when(properties.getSatoY()).thenReturn(13.0f);

		// Array Satellites
		Satellite[] satellites = new Satellite[] { kenobi, sato, skywalker };

		// Request
		TopSecretChallengeAPIRequest request = new TopSecretChallengeAPIRequest(satellites);

		// Response
		ChallengeAPIResponse dtoResp = service.topSecret(request);
		when(properties.getErrorCode()).thenReturn(404);
		assertEquals(properties.getErrorCode(), dtoResp.getResponseCode());

	}

	@Test
	public void topSecretSatelliteAllowedHandlerTest() {

		// Kenobi
		Satellite kenobi = new Satellite("kenobi", 5, new String[] { "este", "", "un", "", "" });
		when(properties.getKenobiX()).thenReturn(5.0f);
		when(properties.getKenobiX()).thenReturn(4.0f);

		// Sato
		Satellite skywalker = new Satellite("skywalker", 5, new String[] { "", "es", "", "", "secreto" });
		when(properties.getSkywalkerX()).thenReturn(4.0f);
		when(properties.getSkywalkerX()).thenReturn(-3.0f);

		// Skywalker
		Satellite sato = new Satellite("otroNombre", 13, new String[] { "este", "", "un", "", "" });
		when(properties.getSatoX()).thenReturn(-4.0f);
		when(properties.getSatoY()).thenReturn(13.0f);

		// Array Satellites
		Satellite[] satellites = new Satellite[] { kenobi, sato, skywalker };

		// Request
		TopSecretChallengeAPIRequest request = new TopSecretChallengeAPIRequest(satellites);

		// Response
		ChallengeAPIResponse dtoResp = service.topSecret(request);
		when(properties.getErrorCode()).thenReturn(404);
		assertEquals(properties.getErrorCode(), dtoResp.getResponseCode());

	}

	@Test
	public void topSecretSatelliteDistanceHandlerTest() {

		// Kenobi
		Satellite kenobi = new Satellite("kenobi", 5, new String[] { "este", "", "un", "", "" });
		when(properties.getKenobiX()).thenReturn(5.0f);
		when(properties.getKenobiX()).thenReturn(4.0f);

		// Sato
		Satellite skywalker = new Satellite("skywalker", 5, new String[] { "", "es", "", "", "secreto" });
		when(properties.getSkywalkerX()).thenReturn(4.0f);
		when(properties.getSkywalkerX()).thenReturn(-3.0f);

		// Skywalker
		Satellite sato = new Satellite("sato", 0.0f, new String[] { "este", "", "un", "", "" });
		when(properties.getSatoX()).thenReturn(-4.0f);
		when(properties.getSatoY()).thenReturn(13.0f);

		// Array Satellites
		Satellite[] satellites = new Satellite[] { kenobi, sato, skywalker };

		// Request
		TopSecretChallengeAPIRequest request = new TopSecretChallengeAPIRequest(satellites);

		// Response
		ChallengeAPIResponse dtoResp = service.topSecret(request);
		when(properties.getErrorCode()).thenReturn(404);
		assertEquals(properties.getErrorCode(), dtoResp.getResponseCode());

	}

	@Test
	public void topSecretSatelliteMessageArrayHandlerTest() {

		// Kenobi
		Satellite kenobi = new Satellite("kenobi", 5, new String[] { "este", "", "un", "", "" });
		when(properties.getKenobiX()).thenReturn(5.0f);
		when(properties.getKenobiX()).thenReturn(4.0f);

		// Sato
		Satellite skywalker = new Satellite("skywalker", 5, new String[] { "", "es", "", "", "secreto" });
		when(properties.getSkywalkerX()).thenReturn(4.0f);
		when(properties.getSkywalkerX()).thenReturn(-3.0f);

		// Skywalker
		Satellite sato = new Satellite("sato", 0.0f, null);
		when(properties.getSatoX()).thenReturn(-4.0f);
		when(properties.getSatoY()).thenReturn(13.0f);

		// Array Satellites
		Satellite[] satellites = new Satellite[] { kenobi, sato, skywalker };

		// Request
		TopSecretChallengeAPIRequest request = new TopSecretChallengeAPIRequest(satellites);

		// Response
		ChallengeAPIResponse dtoResp = service.topSecret(request);
		when(properties.getErrorCode()).thenReturn(404);
		assertEquals(properties.getErrorCode(), dtoResp.getResponseCode());

	}
	
	@Test
	public void topSecretSplitSatelliteInfoHandlerTest() {

		// Name
		String satelliteName = "kenobi";

		// Request
		Satellite satellite = new Satellite(null, 5, new String[] { "este", "", "un", "", "" });

		// Response
		ChallengeAPIResponse dtoResp = service.topSecretSplit(satellite, satelliteName);
		when(properties.getSuccessCode()).thenReturn(200);
		assertEquals(properties.getSuccessCode(), dtoResp.getResponseCode());

	}
	
	@Test
	public void topSecretSplitSatelliteNameEmptyHandlerTest() {

		// Name
		String satelliteName = "";

		// Request
		Satellite satellite = new Satellite(null, 5, new String[] { "este", "", "un", "", "" });

		// Response
		ChallengeAPIResponse dtoResp = service.topSecretSplit(satellite, satelliteName);
		when(properties.getErrorCode()).thenReturn(404);
		assertEquals(properties.getErrorCode(), dtoResp.getResponseCode());

	}
	
	@Test
	public void topSecretSplitSatelliteNameNotAllowedHandlerTest() {

		// Name
		String satelliteName = "otroSatellite";

		// Request
		Satellite satellite = new Satellite(null, 5, new String[] { "este", "", "un", "", "" });

		// Response
		ChallengeAPIResponse dtoResp = service.topSecretSplit(satellite, satelliteName);
		when(properties.getErrorCode()).thenReturn(404);
		assertEquals(properties.getErrorCode(), dtoResp.getResponseCode());

	}
	
	@Test
	public void topSecretSplitSatelliteDistanceInvalidHandlerTest() {

		// Name
		String satelliteName = "kenobi";

		// Request
		Satellite satellite = new Satellite(null, 0, new String[] { "este", "", "un", "", "" });

		// Response
		ChallengeAPIResponse dtoResp = service.topSecretSplit(satellite, satelliteName);
		when(properties.getErrorCode()).thenReturn(404);
		assertEquals(properties.getErrorCode(), dtoResp.getResponseCode());

	}
	
	@Test
	public void topSecretSplitSatelliteDistanceNegativeValueHandlerTest() {

		// Name
		String satelliteName = "kenobi";

		// Request
		Satellite satellite = new Satellite(null, -9, new String[] { "este", "", "un", "", "" });

		// Response
		ChallengeAPIResponse dtoResp = service.topSecretSplit(satellite, satelliteName);
		when(properties.getErrorCode()).thenReturn(404);
		assertEquals(properties.getErrorCode(), dtoResp.getResponseCode());

	}
	
	@Test
	public void topSecretSplitSatelliteMessageNullHandlerTest() {

		// Name
		String satelliteName = "kenobi";

		// Request
		Satellite satellite = new Satellite(null, 5, null);

		// Response
		ChallengeAPIResponse dtoResp = service.topSecretSplit(satellite, satelliteName);
		when(properties.getErrorCode()).thenReturn(404);
		assertEquals(properties.getErrorCode(), dtoResp.getResponseCode());

	}
	
}

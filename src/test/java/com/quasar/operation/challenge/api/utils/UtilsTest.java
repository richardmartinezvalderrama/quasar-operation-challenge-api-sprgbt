package com.quasar.operation.challenge.api.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.quasar.operation.challenge.api.dto.lib.ChallengeAPIResponse;
import com.quasar.operation.challenge.api.dto.lib.Position;
import com.quasar.operation.challenge.api.properties.QuasarOperationChallengeAPIProperties;

public class UtilsTest {

	@Mock
	QuasarOperationChallengeAPIProperties properties;

	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void buildingResponseNullTest() {

		properties = null;

		assertThrows(NullPointerException.class,
				() -> new ChallengeAPIResponse(properties.getSuccessCode(), properties.getSuccessMsg()),
				"It was not possible to load the configuration property");
	}

	@Test
	public void buildingResponseNotNullTest() {

		when(properties.getSuccessCode()).thenReturn(200);
		when(properties.getSuccessMsg()).thenReturn("Successful Operation");

		ChallengeAPIResponse responseDto = new ChallengeAPIResponse(properties.getSuccessCode(),
				properties.getSuccessMsg());
		assertNotNull(responseDto, "ChallengeAPIResponse is not null");
	}
	
	
	@Test
	public void calculatePointIntersectionBetweenSatellitesNullTest() {
		
		// x0, y0 -> r0
		double x0 = -500; double y0 = -200; double r0 = 100;
		
		// x1, y1 -> r1
		double x1 = 100; double y1 = -100; double r1 = 115.5;
		
		// x2, y2 -> r2
		double x2 = 500; double y2 = 100; double r2 = 142.7;
		
		Position position = Utils.calculatePointIntersectionBetweenSatellites(x0, y0, r0, x1, y1, r1, x2, y2, r2);
		assertNull(position, "Position is not null");
	}
	
	
	@Test
	public void calculatePointIntersectionBetweenSatellitesNotNullTest() {
		
		// x0, y0 -> r0
		double x0 = 5; double y0 = 4; double r0 = 5;
		
		// x1, y1 -> r1
		double x1 = 4; double y1 = -3; double r1 = 5;
		
		// x2, y2 -> r2
		double x2 = -4; double y2 = 13; double r2 = 13;
		
		Position position = Utils.calculatePointIntersectionBetweenSatellites(x0, y0, r0, x1, y1, r1, x2, y2, r2);
		assertNotNull(position, "Position is not null");
	}
	
	
	@Test
	public void calculatePointIntersectionBetweenSatellitesTest() {
		
		// x0, y0 -> r0
		double x0 = -500; double y0 = -200; double r0 = 485.41;
		
		// x1, y1 -> r1
		double x1 = 100; double y1 = -100; double r1 = 265.75;
		
		// x2, y2 -> r2
		double x2 = 500; double y2 = 100; double r2 = 600.52;
		
		Position position = Utils.calculatePointIntersectionBetweenSatellites(x0, y0, r0, x1, y1, r1, x2, y2, r2);
		assertNotNull(position, "Position is not null");
	}
	
	
	@Test
	public void calculatePointIntersectionBetweenSatellitesPointValuesTest() {
		
		// x0, y0 -> r0
		double x0 = -500; double y0 = -200; double r0 = 485.41;
		
		// x1, y1 -> r1
		double x1 = 100; double y1 = -100; double r1 = 265.75;
		
		// x2, y2 -> r2
		double x2 = 500; double y2 = 100; double r2 = 600.52;
		
		Position position = Utils.calculatePointIntersectionBetweenSatellites(x0, y0, r0, x1, y1, r1, x2, y2, r2);
		assertEquals(-100.0, position.getX(), 0.1);
		assertEquals(75.0, position.getY(), 0.1);
	}

}

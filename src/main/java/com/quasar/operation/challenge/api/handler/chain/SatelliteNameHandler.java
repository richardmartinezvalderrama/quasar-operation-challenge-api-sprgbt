package com.quasar.operation.challenge.api.handler.chain;

import org.apache.commons.lang3.StringUtils;

import com.quasar.operation.challenge.api.dto.lib.ChallengeAPIResponse;
import com.quasar.operation.challenge.api.dto.lib.Satellite;
import com.quasar.operation.challenge.api.properties.QuasarOperationChallengeAPIProperties;
import com.quasar.operation.challenge.api.utils.Utils;

public class SatelliteNameHandler extends AbstractFilterSatelliteHandler {

	@Override
	public ChallengeAPIResponse performValidation(Satellite satellite,
			QuasarOperationChallengeAPIProperties properties) {

		if (StringUtils.isBlank(satellite.getName())) {
			LOGGER.warn(Utils.LOG_BAD_REQUEST_NULL_SATELLITE_NAME);
			return Utils.buildingResponse(properties.getErrorCode(), properties.getBadSatelliteNameMsg());

		} else {
			return nextValidation.performValidation(satellite, properties);
		}
	}

}

package com.quasar.operation.challenge.api.handler.chain;

import com.quasar.operation.challenge.api.dto.lib.ChallengeAPIResponse;
import com.quasar.operation.challenge.api.dto.lib.Satellite;
import com.quasar.operation.challenge.api.properties.QuasarOperationChallengeAPIProperties;
import com.quasar.operation.challenge.api.utils.Utils;

public class SatelliteInfoHandler extends AbstractFilterArraySatelliteHandler {

	@Override
	public ChallengeAPIResponse performValidation(Satellite[] satellites,
			QuasarOperationChallengeAPIProperties properties) {

		ChallengeAPIResponse dtoResp = null;

		for (Satellite satellite : satellites) {

			if ((dtoResp = checkSatelliteInfo(satellite, properties)) != null
					&& !properties.getSuccessCode().equals(dtoResp.getResponseCode())) {
				LOGGER.warn(Utils.LOG_ERROR_METHOD, Utils.METHOD_CHECK_SATELLITE_INFO, dtoResp.getResponseCode(),
						dtoResp.getResponseMessage());
				break;
			}
		}

		return dtoResp;
	}

	/**
	 * Method in charge of validating the information corresponding to the
	 * attributes of a satellite such as the name, the distance and the message
	 * 
	 * @param satellite information
	 * @return ChallengeAPIResponse: It contains a code and a response message
	 *         associated with the validation
	 */
	public ChallengeAPIResponse checkSatelliteInfo(Satellite satellite,
			QuasarOperationChallengeAPIProperties properties) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_CHECK_SATELLITE_INFO);

		ChallengeAPIResponse dtoResp = null;

		LOGGER.debug(Utils.LOG_DEFINE_CONCRETE_HANDLERS);
		AbstractFilterSatelliteHandler satelliteNameHandler = new SatelliteNameHandler();
		AbstractFilterSatelliteHandler satelliteAllowedHandler = new SatelliteAllowedHandler();
		AbstractFilterSatelliteHandler satelliteDistanceHandler = new SatelliteDistanceHandler();
		AbstractFilterSatelliteHandler satelliteMessageArrayHandler = new SatelliteMessageArrayHandler();

		LOGGER.debug(Utils.LOG_DEFINE_CHAIN_OF_RESPONSIBILITY_HANDLERS);
		satelliteNameHandler.setNextValidation(satelliteAllowedHandler);
		satelliteAllowedHandler.setNextValidation(satelliteDistanceHandler);
		satelliteDistanceHandler.setNextValidation(satelliteMessageArrayHandler);

		LOGGER.debug(Utils.LOG_START_SPECIFIC_HANDLER_IN_CHARGE_OF_PERFORM_VALIDATIONS);
		dtoResp = satelliteNameHandler.performValidation(satellite, properties);

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_CHECK_SATELLITE_INFO);
		return dtoResp;
	}

}

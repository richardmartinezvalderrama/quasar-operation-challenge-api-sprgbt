package com.quasar.operation.challenge.api.handler.chain;

import org.apache.commons.lang3.ArrayUtils;

import com.quasar.operation.challenge.api.dto.lib.ChallengeAPIResponse;
import com.quasar.operation.challenge.api.dto.lib.Satellite;
import com.quasar.operation.challenge.api.properties.QuasarOperationChallengeAPIProperties;
import com.quasar.operation.challenge.api.utils.Utils;

public class SatelliteMessageArrayHandler extends AbstractFilterSatelliteHandler {

	@Override
	public ChallengeAPIResponse performValidation(Satellite satellite,
			QuasarOperationChallengeAPIProperties properties) {

		if (ArrayUtils.isEmpty(satellite.getMessage())) {
			LOGGER.warn(Utils.LOG_BAD_REQUEST_BAD_SATELLITE_MESSAGE);
			return Utils.buildingResponse(properties.getErrorCode(), properties.getBadSatelliteMessageMsg());
		}

		return Utils.buildingResponse(properties.getSuccessCode(), properties.getBadSatelliteMessageMsg());
	}

}

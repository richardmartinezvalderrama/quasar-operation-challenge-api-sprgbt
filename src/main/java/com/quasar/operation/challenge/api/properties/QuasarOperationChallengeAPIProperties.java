package com.quasar.operation.challenge.api.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "challenge.api")
public class QuasarOperationChallengeAPIProperties {

	// Message
	private String message;

	// Split Regex
	private String splitRegex;

	// Number of satellites to position
	private Integer numberSatellites;

	// Name of satellites to position
	private String nameSatellites;

	// Kenobi:[-500, -200] satellite position
	private float kenobiX;
	private float kenobiY;

	// Skywalker:[100, -100] satellite position
	private float skywalkerX;
	private float skywalkerY;

	// Sato:[500, 100] satellite position
	private float satoX;
	private float satoY;

	// Successful response
	private Integer successCode;
	private String successMsg;
	private String validSuccessMsg;

	// Bad response
	private Integer errorCode;
	private String badArraySatellitesMsg;
	private String badInvalidNumberSatellitesMsg;
	private String badSatelliteNameMsg;
	private String badSatelliteDistanceMsg;
	private String badSatelliteMessageMsg;
	private String errorGettingPositionsMsg;
	private String errorLengthDifferentMsg;
	private String errorPositionNotFoundMsg;
	private String errorNotEnoughInfoMsg;
	private String exceptionMsg;

	
	/*
	 * Getter & Setter
	 */
	
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the splitRegex
	 */
	public String getSplitRegex() {
		return splitRegex;
	}

	/**
	 * @param splitRegex the splitRegex to set
	 */
	public void setSplitRegex(String splitRegex) {
		this.splitRegex = splitRegex;
	}

	/**
	 * @return the numberSatellites
	 */
	public Integer getNumberSatellites() {
		return numberSatellites;
	}

	/**
	 * @param numberSatellites the numberSatellites to set
	 */
	public void setNumberSatellites(Integer numberSatellites) {
		this.numberSatellites = numberSatellites;
	}

	/**
	 * @return the nameSatellites
	 */
	public String getNameSatellites() {
		return nameSatellites;
	}

	/**
	 * @param nameSatellites the nameSatellites to set
	 */
	public void setNameSatellites(String nameSatellites) {
		this.nameSatellites = nameSatellites;
	}

	/**
	 * @return the kenobiX
	 */
	public float getKenobiX() {
		return kenobiX;
	}

	/**
	 * @param kenobiX the kenobiX to set
	 */
	public void setKenobiX(float kenobiX) {
		this.kenobiX = kenobiX;
	}

	/**
	 * @return the kenobiY
	 */
	public float getKenobiY() {
		return kenobiY;
	}

	/**
	 * @param kenobiY the kenobiY to set
	 */
	public void setKenobiY(float kenobiY) {
		this.kenobiY = kenobiY;
	}

	/**
	 * @return the skywalkerX
	 */
	public float getSkywalkerX() {
		return skywalkerX;
	}

	/**
	 * @param skywalkerX the skywalkerX to set
	 */
	public void setSkywalkerX(float skywalkerX) {
		this.skywalkerX = skywalkerX;
	}

	/**
	 * @return the skywalkerY
	 */
	public float getSkywalkerY() {
		return skywalkerY;
	}

	/**
	 * @param skywalkerY the skywalkerY to set
	 */
	public void setSkywalkerY(float skywalkerY) {
		this.skywalkerY = skywalkerY;
	}

	/**
	 * @return the satoX
	 */
	public float getSatoX() {
		return satoX;
	}

	/**
	 * @param satoX the satoX to set
	 */
	public void setSatoX(float satoX) {
		this.satoX = satoX;
	}

	/**
	 * @return the satoY
	 */
	public float getSatoY() {
		return satoY;
	}

	/**
	 * @param satoY the satoY to set
	 */
	public void setSatoY(float satoY) {
		this.satoY = satoY;
	}

	/**
	 * @return the successCode
	 */
	public Integer getSuccessCode() {
		return successCode;
	}

	/**
	 * @param successCode the successCode to set
	 */
	public void setSuccessCode(Integer successCode) {
		this.successCode = successCode;
	}

	/**
	 * @return the successMsg
	 */
	public String getSuccessMsg() {
		return successMsg;
	}

	/**
	 * @param successMsg the successMsg to set
	 */
	public void setSuccessMsg(String successMsg) {
		this.successMsg = successMsg;
	}

	/**
	 * @return the validSuccessMsg
	 */
	public String getValidSuccessMsg() {
		return validSuccessMsg;
	}

	/**
	 * @param validSuccessMsg the validSuccessMsg to set
	 */
	public void setValidSuccessMsg(String validSuccessMsg) {
		this.validSuccessMsg = validSuccessMsg;
	}

	/**
	 * @return the errorCode
	 */
	public Integer getErrorCode() {
		return errorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * @return the badArraySatellitesMsg
	 */
	public String getBadArraySatellitesMsg() {
		return badArraySatellitesMsg;
	}

	/**
	 * @param badArraySatellitesMsg the badArraySatellitesMsg to set
	 */
	public void setBadArraySatellitesMsg(String badArraySatellitesMsg) {
		this.badArraySatellitesMsg = badArraySatellitesMsg;
	}

	/**
	 * @return the badInvalidNumberSatellitesMsg
	 */
	public String getBadInvalidNumberSatellitesMsg() {
		return badInvalidNumberSatellitesMsg;
	}

	/**
	 * @param badInvalidNumberSatellitesMsg the badInvalidNumberSatellitesMsg to set
	 */
	public void setBadInvalidNumberSatellitesMsg(String badInvalidNumberSatellitesMsg) {
		this.badInvalidNumberSatellitesMsg = badInvalidNumberSatellitesMsg;
	}

	/**
	 * @return the badSatelliteNameMsg
	 */
	public String getBadSatelliteNameMsg() {
		return badSatelliteNameMsg;
	}

	/**
	 * @param badSatelliteNameMsg the badSatelliteNameMsg to set
	 */
	public void setBadSatelliteNameMsg(String badSatelliteNameMsg) {
		this.badSatelliteNameMsg = badSatelliteNameMsg;
	}

	/**
	 * @return the badSatelliteDistanceMsg
	 */
	public String getBadSatelliteDistanceMsg() {
		return badSatelliteDistanceMsg;
	}

	/**
	 * @param badSatelliteDistanceMsg the badSatelliteDistanceMsg to set
	 */
	public void setBadSatelliteDistanceMsg(String badSatelliteDistanceMsg) {
		this.badSatelliteDistanceMsg = badSatelliteDistanceMsg;
	}

	/**
	 * @return the badSatelliteMessageMsg
	 */
	public String getBadSatelliteMessageMsg() {
		return badSatelliteMessageMsg;
	}

	/**
	 * @param badSatelliteMessageMsg the badSatelliteMessageMsg to set
	 */
	public void setBadSatelliteMessageMsg(String badSatelliteMessageMsg) {
		this.badSatelliteMessageMsg = badSatelliteMessageMsg;
	}

	/**
	 * @return the errorGettingPositionsMsg
	 */
	public String getErrorGettingPositionsMsg() {
		return errorGettingPositionsMsg;
	}

	/**
	 * @param errorGettingPositionsMsg the errorGettingPositionsMsg to set
	 */
	public void setErrorGettingPositionsMsg(String errorGettingPositionsMsg) {
		this.errorGettingPositionsMsg = errorGettingPositionsMsg;
	}

	/**
	 * @return the errorLengthDifferentMsg
	 */
	public String getErrorLengthDifferentMsg() {
		return errorLengthDifferentMsg;
	}

	/**
	 * @param errorLengthDifferentMsg the errorLengthDifferentMsg to set
	 */
	public void setErrorLengthDifferentMsg(String errorLengthDifferentMsg) {
		this.errorLengthDifferentMsg = errorLengthDifferentMsg;
	}

	/**
	 * @return the errorPositionNotFoundMsg
	 */
	public String getErrorPositionNotFoundMsg() {
		return errorPositionNotFoundMsg;
	}

	/**
	 * @param errorPositionNotFoundMsg the errorPositionNotFoundMsg to set
	 */
	public void setErrorPositionNotFoundMsg(String errorPositionNotFoundMsg) {
		this.errorPositionNotFoundMsg = errorPositionNotFoundMsg;
	}

	/**
	 * @return the errorNotEnoughInfoMsg
	 */
	public String getErrorNotEnoughInfoMsg() {
		return errorNotEnoughInfoMsg;
	}

	/**
	 * @param errorNotEnoughInfoMsg the errorNotEnoughInfoMsg to set
	 */
	public void setErrorNotEnoughInfoMsg(String errorNotEnoughInfoMsg) {
		this.errorNotEnoughInfoMsg = errorNotEnoughInfoMsg;
	}

	/**
	 * @return the exceptionMsg
	 */
	public String getExceptionMsg() {
		return exceptionMsg;
	}

	/**
	 * @param exceptionMsg the exceptionMsg to set
	 */
	public void setExceptionMsg(String exceptionMsg) {
		this.exceptionMsg = exceptionMsg;
	}

}

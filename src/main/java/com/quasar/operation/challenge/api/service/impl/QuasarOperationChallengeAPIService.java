package com.quasar.operation.challenge.api.service.impl;

import java.util.Arrays;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.quasar.operation.challenge.api.dto.lib.ChallengeAPIResponse;
import com.quasar.operation.challenge.api.dto.lib.Position;
import com.quasar.operation.challenge.api.dto.lib.Satellite;
import com.quasar.operation.challenge.api.dto.lib.TopSecretChallengeAPIRequest;
import com.quasar.operation.challenge.api.handler.chain.AbstractFilterArraySatelliteHandler;
import com.quasar.operation.challenge.api.handler.chain.MessageArrayHandler;
import com.quasar.operation.challenge.api.handler.chain.MessageLengthHandler;
import com.quasar.operation.challenge.api.handler.chain.SatelliteArrayHandler;
import com.quasar.operation.challenge.api.handler.chain.SatelliteInfoHandler;
import com.quasar.operation.challenge.api.handler.chain.SatelliteNumberHandler;
import com.quasar.operation.challenge.api.properties.QuasarOperationChallengeAPIProperties;
import com.quasar.operation.challenge.api.service.IQuasarOperationChallengeAPI;
import com.quasar.operation.challenge.api.utils.Utils;

@Service
public class QuasarOperationChallengeAPIService implements IQuasarOperationChallengeAPI {

	private static final Logger LOGGER = LoggerFactory.getLogger(QuasarOperationChallengeAPIService.class);

	@Autowired
	QuasarOperationChallengeAPIProperties properties;

	private Satellite[] satellitesArray = null;

	/*
	 * Constructor
	 */
	@PostConstruct
	public void PostConstructQuasarOperationChallengeAPIService() {
		this.satellitesArray = new Satellite[properties.getNumberSatellites()];
	}

	/**
	 * TopSecret
	 * 
	 * Service in charge of implementing the logic to obtain the location and the
	 * message issued by the spaceship
	 * 
	 * @param TopSecretChallengeAPIRequest: It contains an array with the name, the
	 *                                      distance and the message received in
	 *                                      each of the satellites
	 * 
	 * @return ChallengeAPIResponse: Contains the position of the spaceship and the
	 *         message issued by it
	 */
	@Override
	public ChallengeAPIResponse topSecret(TopSecretChallengeAPIRequest request) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_TOP_SECRET);

		ChallengeAPIResponse dtoResp = null;

		LOGGER.debug(Utils.LOG_VALIDATION_FIELDS_RECEIVED_IN_REQUEST_BODY);
		dtoResp = previousValidationsTopSecretRequest(request.getSatellites());
		
		LOGGER.debug(Utils.LOG_CHECK_PERFORM_VALIDATION_REQUEST_BODY, dtoResp.getResponseCode());
		if (properties.getSuccessCode().equals(dtoResp.getResponseCode())) {
			LOGGER.debug(Utils.LOG_PROCESS_TOP_SECRET);
			dtoResp = processTopSecret(request.getSatellites());
		}

		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_TOP_SECRET);
		return dtoResp;
	}

	/**
	 * TopSecret_Split
	 * 
	 * EndPoint in charge of receiving in different POST the information associated
	 * with each satellite
	 * 
	 * @param request:       associated with the satellite (distance and message)
	 * @param satelliteName: associated with the satellite name
	 * 
	 * @return ChallengeAPIResponse: Returns a code and a response message
	 *         indicating that the satellite information was stored correctly
	 */
	@Override
	public ChallengeAPIResponse topSecretSplit(Satellite request, String satelliteName) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_TOP_SECRET_SPLIT);

		ChallengeAPIResponse dtoResp = null;

		LOGGER.debug(Utils.LOG_BUILD_SATELLITE_OBJECT);
		Satellite[] satellites = { new Satellite(satelliteName, request.getDistance(), request.getMessage()) };

		LOGGER.debug(Utils.LOG_VALIDATION_FIELDS_RECEIVED_IN_REQUEST_BODY);
		AbstractFilterArraySatelliteHandler satelliteInfoHandler = new SatelliteInfoHandler();

		LOGGER.debug(Utils.LOG_START_SPECIFIC_HANDLER_IN_CHARGE_OF_PERFORM_VALIDATIONS);
		dtoResp = satelliteInfoHandler.performValidation(satellites, properties);

		LOGGER.debug(Utils.LOG_CHECK_PERFORM_VALIDATION_REQUEST_BODY, dtoResp.getResponseCode());
		if (properties.getSuccessCode().equals(dtoResp.getResponseCode())) {

			if (Utils.KENOBI.equals(satelliteName.toUpperCase())) {
				satellitesArray[0] = satellites[0];

			} else if (Utils.SKYWALKER.equals(satelliteName.toUpperCase())) {
				satellitesArray[1] = satellites[0];

			}
			if (Utils.SATO.equals(satelliteName.toUpperCase())) {
				satellitesArray[2] = satellites[0];
			}

			dtoResp = new ChallengeAPIResponse(properties.getSuccessCode(), properties.getSuccessMsg());
		}

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_TOP_SECRET_SPLIT);
		return dtoResp;
	}

	/**
	 * GetTopSecret_Split
	 * 
	 * EndPoint in charge of obtaining the location and the message that the
	 * spaceship emits
	 * 
	 * @return ChallengeAPIResponse: Contains the position of the spaceship and the
	 *         message issued by it
	 */
	@Override
	public ChallengeAPIResponse getTopSecretSplit() {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_GET_TOP_SECRET_SPLIT);

		ChallengeAPIResponse dtoResp = null;

		LOGGER.debug(Utils.LOG_VALIDATION_FIELDS_RECEIVED_IN_REQUEST_BODY);
		dtoResp = previousValidationsTopSecretRequest(satellitesArray);

		LOGGER.debug(Utils.LOG_CHECK_PERFORM_VALIDATION_REQUEST_BODY, dtoResp.getResponseCode());
		if (properties.getSuccessCode().equals(dtoResp.getResponseCode())) {
			LOGGER.debug(Utils.LOG_PROCESS_TOP_SECRET);
			dtoResp = processTopSecret(satellitesArray);

		} else {
			LOGGER.warn(Utils.LOG_NOT_ENOUGH_INFO_ABOUT_SATELLITES);
			dtoResp = new ChallengeAPIResponse(properties.getErrorCode(), properties.getErrorNotEnoughInfoMsg());
		}

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_GET_TOP_SECRET_SPLIT);
		return dtoResp;
	}

	/*
	 * **************************************************************************
	 * ************************* PRIVATE METHODS ********************************
	 * **************************************************************************
	 */

	/**
	 * Method in charge of carrying out the validation of the fields received in the
	 * request body, in relation to the business rules
	 * 
	 * @param satellites: It contains an array with the name, the distance and the
	 *                    message received in each of the satellites
	 * @return ChallengeAPIResponse: It contains a code and a response message
	 *         associated with the validation
	 */
	private ChallengeAPIResponse previousValidationsTopSecretRequest(Satellite[] satellites) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_PREVIOUS_VALIDATIONS_TOP_SECRET_REQUEST);

		ChallengeAPIResponse dtoResp = null;

		LOGGER.debug(Utils.LOG_DEFINE_CONCRETE_HANDLERS);
		AbstractFilterArraySatelliteHandler satelliteArrayHandler = new SatelliteArrayHandler();
		AbstractFilterArraySatelliteHandler satelliteNumberHandler = new SatelliteNumberHandler();
		AbstractFilterArraySatelliteHandler messageArrayHandler = new MessageArrayHandler();
		AbstractFilterArraySatelliteHandler messageLengthHandler = new MessageLengthHandler();
		AbstractFilterArraySatelliteHandler satelliteInfoHandler = new SatelliteInfoHandler();

		LOGGER.debug(Utils.LOG_DEFINE_CHAIN_OF_RESPONSIBILITY_HANDLERS);
		satelliteArrayHandler.setNextValidation(satelliteNumberHandler);
		satelliteNumberHandler.setNextValidation(messageArrayHandler);
		messageArrayHandler.setNextValidation(messageLengthHandler);
		messageLengthHandler.setNextValidation(satelliteInfoHandler);

		LOGGER.debug(Utils.LOG_START_SPECIFIC_HANDLER_IN_CHARGE_OF_PERFORM_VALIDATIONS);
		dtoResp = satelliteArrayHandler.performValidation(satellites, properties);

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_PREVIOUS_VALIDATIONS_TOP_SECRET_REQUEST);
		return dtoResp;

	}

	/**
	 * Process to obtain the location and the message issued by the spaceship
	 * 
	 * @param satellites: It contains an array with the name, the distance and the
	 *                    message received in each of the satellites
	 * 
	 * @return ChallengeAPIResponse: A code and a response message accompanied by
	 *         the position of the spaceship and the message issued by it, in case
	 *         the operation is carried out success
	 */
	private ChallengeAPIResponse processTopSecret(Satellite[] satellites) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_PROCESS_TOP_SECRET);

		ChallengeAPIResponse dtoResp = null;

		Position position = null;
		String message = null;

		LOGGER.debug(Utils.LOG_SATELLITES_ARE_ORDERED);
		Arrays.sort(satellites);
		LOGGER.debug(Arrays.toString(satellites));

		LOGGER.debug(Utils.LOG_GET_POSITION_SPACESHIP);
		position = getLocation(satellites[0].getDistance(), satellites[1].getDistance(), satellites[2].getDistance());

		LOGGER.debug(Utils.LOG_GET_MESSAGE_SPACESHIP);
		message = getMessage(satellites[0].getMessage(), satellites[1].getMessage(), satellites[2].getMessage());

		if (position != null && StringUtils.isNotBlank(message)) {

			dtoResp = new ChallengeAPIResponse(properties.getSuccessCode(), properties.getSuccessMsg(), position, message);

		} else {
			dtoResp = new ChallengeAPIResponse(properties.getErrorCode(), properties.getErrorPositionNotFoundMsg());
		}

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_PROCESS_TOP_SECRET);
		return dtoResp;
	}

	/**
	 * Method in charge of calculating and obtaining the position of the spaceship
	 * 
	 * @param kenobiDistance:    distance from spaceship to satellite
	 * @param satoDistance:      distance from spaceship to satellite
	 * @param skywalkerDistance: distance from spaceship to satellite
	 * @return Position: position corresponding to the location of the spaceship
	 */
	private Position getLocation(float kenobiDistance, float satoDistance, float skywalkerDistance) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_GET_LOCATION);

		Position position = null;

		/*
		 * Read from properties the coordinates related to the location of the
		 * satellites
		 */
		double kenobiX = Double.parseDouble(new Float(properties.getKenobiX()).toString());
		double kenobiY = Double.parseDouble(new Float(properties.getKenobiY()).toString());
		double kenobiRadius = Double.parseDouble(new Float(kenobiDistance).toString());

		double satoX = Double.parseDouble(new Float(properties.getSatoX()).toString());
		double satoY = Double.parseDouble(new Float(properties.getSatoY()).toString());
		double satoRadius = Double.parseDouble(new Float(satoDistance).toString());

		double skywalkerX = Double.parseDouble(new Float(properties.getSkywalkerX()).toString());
		double skywalkerY = Double.parseDouble(new Float(properties.getSkywalkerY()).toString());
		double skywalkerRadius = Double.parseDouble(new Float(skywalkerDistance).toString());

		LOGGER.debug(Utils.LOG_CALCULATE_POINT_INTERSECTION_BETWEEN_SATELLITES);
		position = Utils.calculatePointIntersectionBetweenSatellites(kenobiX, kenobiY, kenobiRadius, satoX, satoY,
				satoRadius, skywalkerX, skywalkerY, skywalkerRadius);

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_GET_LOCATION);
		return position;
	}

	/**
	 * Method in charge of constructing the message emitted by the spaceship
	 * 
	 * It is assumed that the array corresponding to the message received in each
	 * spaceship will have the same length
	 * 
	 * It is assumed that each word belonging to the message of the array has a
	 * unique position given within the array of the message and that also when the
	 * word cannot be determined, a blank space will be received for said word.
	 * Which implies that at no point the order of the words associated with the
	 * message changes
	 * 
	 * @param kenobiMessages:    message received on the satellite
	 * @param skywalkerMessages: message received on the satellite
	 * @param satoMessages:      message received on the satellite
	 * @return joinedMsg emitted by the spaceship
	 */
	private String getMessage(String[] kenobiMessages, String[] skywalkerMessages, String[] satoMessages) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_GET_MESSAGE);

		String[] arrMsg = new String[kenobiMessages.length];
		String joinedMsg = "";

		for (int i = 0; i < arrMsg.length; i++) {

			if (StringUtils.isNotBlank(kenobiMessages[i])) {
				joinedMsg = kenobiMessages[i];
			}
			if (StringUtils.isNotBlank(skywalkerMessages[i])) {
				joinedMsg = skywalkerMessages[i];
			}
			if (StringUtils.isNotBlank(satoMessages[i])) {
				joinedMsg = satoMessages[i];
			}

			arrMsg[i] = joinedMsg;
		}

		joinedMsg = String.join(" ", arrMsg);

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_GET_MESSAGE);
		return joinedMsg;
	}

}

package com.quasar.operation.challenge.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuasarOperationChallengeApiSprgbtApp {

	private static final Logger LOGGER = LoggerFactory.getLogger(QuasarOperationChallengeApiSprgbtApp.class);

	public static void main(String[] args) {
		
		LOGGER.info("*** Starting Quasar Operation Challenge API REST ***");
		SpringApplication.run(QuasarOperationChallengeApiSprgbtApp.class, args);
	}

}

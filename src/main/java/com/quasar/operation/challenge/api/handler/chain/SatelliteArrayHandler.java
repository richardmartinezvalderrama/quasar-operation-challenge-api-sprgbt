package com.quasar.operation.challenge.api.handler.chain;

import org.apache.commons.lang3.ArrayUtils;

import com.quasar.operation.challenge.api.dto.lib.ChallengeAPIResponse;
import com.quasar.operation.challenge.api.dto.lib.Satellite;
import com.quasar.operation.challenge.api.properties.QuasarOperationChallengeAPIProperties;
import com.quasar.operation.challenge.api.utils.Utils;

public class SatelliteArrayHandler extends AbstractFilterArraySatelliteHandler {

	@Override
	public ChallengeAPIResponse performValidation(Satellite[] satellites,
			QuasarOperationChallengeAPIProperties properties) {
		
		if (ArrayUtils.isEmpty(satellites)) {
			LOGGER.warn(Utils.LOG_BAD_REQUEST_ARRAY_SATELLITES_IS_EMPTY);
			return Utils.buildingResponse(properties.getErrorCode(), properties.getBadArraySatellitesMsg());
			
		} else {
			return nextValidation.performValidation(satellites, properties);
		}
	}

}

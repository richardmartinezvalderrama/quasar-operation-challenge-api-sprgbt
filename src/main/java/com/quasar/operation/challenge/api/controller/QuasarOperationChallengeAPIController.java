package com.quasar.operation.challenge.api.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.quasar.operation.challenge.api.dto.lib.ChallengeAPIResponse;
import com.quasar.operation.challenge.api.dto.lib.Satellite;
import com.quasar.operation.challenge.api.dto.lib.TopSecretChallengeAPIRequest;
import com.quasar.operation.challenge.api.properties.QuasarOperationChallengeAPIProperties;
import com.quasar.operation.challenge.api.service.IQuasarOperationChallengeAPI;
import com.quasar.operation.challenge.api.utils.Utils;

@RestController
public class QuasarOperationChallengeAPIController {

	private static final Logger LOGGER = LoggerFactory.getLogger(QuasarOperationChallengeAPIController.class);

	@Autowired
	QuasarOperationChallengeAPIProperties properties;

	@Autowired
	IQuasarOperationChallengeAPI service;

	/**
	 * TopSecret
	 * 
	 * EndPoint in charge of obtaining the location and the message that the
	 * spaceship emits
	 * 
	 * @param TopSecretChallengeAPIRequest: It contains an array with the name, the
	 *                                      distance and the message received in
	 *                                      each of the satellites
	 * 
	 * @return ChallengeAPIResponse: Contains the position of the spaceship and the
	 *         message issued by it
	 */
	@PostMapping("/topsecret")
	public ResponseEntity<ChallengeAPIResponse> topSecret(@Valid @RequestBody TopSecretChallengeAPIRequest request) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_TOP_SECRET);

		ResponseEntity<ChallengeAPIResponse> wsResponse = null;
		ChallengeAPIResponse dtoResp = null;

		try {

			LOGGER.debug(Utils.LOG_GET_LOCATION_AND_MESSAGE_FROM_SPACESHIP);
			dtoResp = service.topSecret(request);

			LOGGER.debug(Utils.LOG_BUILDING_RESPONSE_METHOD, Utils.METHOD_TOP_SECRET);
			wsResponse = new ResponseEntity<>(dtoResp, HttpStatus.OK);

		} catch (Exception ex) {
			LOGGER.error(Utils.LOG_EXCEPTION, ex);
			dtoResp = new ChallengeAPIResponse(properties.getErrorCode(), properties.getExceptionMsg());

			LOGGER.debug(Utils.LOG_BUILDING_RESPONSE_METHOD, Utils.METHOD_TOP_SECRET);
			wsResponse = new ResponseEntity<>(dtoResp, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		LOGGER.info(Utils.LOG_TOP_SECRET_METHOD, dtoResp.getResponseCode(), dtoResp.getResponseMessage());

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_TOP_SECRET);
		return wsResponse;
	}

	/**
	 * TopSecret_Split
	 * 
	 * EndPoint in charge of receiving in different POST the information associated
	 * with each satellite
	 * 
	 * @param request:       associated with the satellite (distance and message)
	 * @param satelliteName: associated with the satellite name
	 * 
	 * @return ChallengeAPIResponse: Returns a code and a response message
	 *         indicating that the satellite information was stored correctly
	 */
	@PostMapping("/topsecret_split/{satellite_name}")
	public ResponseEntity<ChallengeAPIResponse> topSecretSplit(@Valid @RequestBody Satellite request,
			@PathVariable(name = "satellite_name", required = true) String satelliteName) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_TOP_SECRET_SPLIT);

		ResponseEntity<ChallengeAPIResponse> wsResponse = null;
		ChallengeAPIResponse dtoResp = null;

		try {

			LOGGER.debug(Utils.LOG_GET_LOCATION_AND_MESSAGE_FROM_SPACESHIP);
			dtoResp = service.topSecretSplit(request, satelliteName);

			LOGGER.debug(Utils.LOG_BUILDING_RESPONSE_METHOD, Utils.METHOD_TOP_SECRET_SPLIT);
			wsResponse = new ResponseEntity<>(dtoResp, HttpStatus.OK);

		} catch (Exception ex) {
			LOGGER.error(Utils.LOG_EXCEPTION, ex);
			dtoResp = new ChallengeAPIResponse(properties.getErrorCode(), properties.getExceptionMsg());

			LOGGER.debug(Utils.LOG_BUILDING_RESPONSE_METHOD, Utils.METHOD_TOP_SECRET);
			wsResponse = new ResponseEntity<>(dtoResp, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		LOGGER.info(Utils.LOG_TOP_SECRET_SPLIT_METHOD, dtoResp.getResponseCode(), dtoResp.getResponseMessage());

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_TOP_SECRET_SPLIT);
		return wsResponse;
	}

	/**
	 * GetTopSecret_Split
	 * 
	 * EndPoint in charge of obtaining the location and the message that the
	 * spaceship emits
	 * 
	 * @return ChallengeAPIResponse: Contains the position of the spaceship and the
	 *         message issued by it
	 */
	@GetMapping("/topsecret_split")
	public ResponseEntity<ChallengeAPIResponse> getTopSecretSplit() {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_GET_TOP_SECRET_SPLIT);

		ResponseEntity<ChallengeAPIResponse> wsResponse = null;
		ChallengeAPIResponse dtoResp = null;

		try {

			LOGGER.debug(Utils.LOG_GET_LOCATION_AND_MESSAGE_FROM_SPACESHIP);
			dtoResp = service.getTopSecretSplit();

			LOGGER.debug(Utils.LOG_BUILDING_RESPONSE_METHOD, Utils.METHOD_GET_TOP_SECRET_SPLIT);
			wsResponse = new ResponseEntity<>(dtoResp, HttpStatus.OK);

		} catch (Exception ex) {
			LOGGER.error(Utils.LOG_EXCEPTION, ex);
			dtoResp = new ChallengeAPIResponse(properties.getErrorCode(), properties.getExceptionMsg());

			LOGGER.debug(Utils.LOG_BUILDING_RESPONSE_METHOD, Utils.METHOD_GET_TOP_SECRET_SPLIT);
			wsResponse = new ResponseEntity<>(dtoResp, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		LOGGER.info(Utils.LOG_GET_TOP_SECRET_SPLIT_METHOD, dtoResp.getResponseCode(), dtoResp.getResponseMessage());

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_GET_TOP_SECRET_SPLIT);
		return wsResponse;
	}

}

package com.quasar.operation.challenge.api.exception;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class QuasarOperationChallengeAPIException extends Exception {

	private static final long serialVersionUID = 1L;

	private static final DateTimeFormatter DF = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	private Integer responseCode;
	private String responseMessage;
	private String details;
	private String date;

	/**
	 * QuasarOperationChallengeAPIException
	 * 
	 * @param code
	 * @param message
	 * @param date
	 */
	public QuasarOperationChallengeAPIException(Integer code, String message, LocalDateTime date) {
		this.responseCode = code;
		this.responseMessage = message;
		this.date = date.format(DF);
	}

	/**
	 * QuasarOperationChallengeAPIException
	 * 
	 * @param code
	 * @param message
	 * @param date
	 * @param cause
	 */
	public QuasarOperationChallengeAPIException(Integer code, String message, LocalDateTime date, Throwable cause) {
		super(cause);
		this.responseCode = code;
		this.responseMessage = message;
		this.date = date.format(DF);
	}

	/**
	 * QuasarOperationChallengeAPIException
	 * 
	 * @param code
	 * @param message
	 * @param details
	 * @param date
	 */
	public QuasarOperationChallengeAPIException(Integer code, String message, String details, LocalDateTime date) {
		this.responseCode = code;
		this.responseMessage = message;
		this.details = details;
		this.date = date.format(DF);
	}

	/**
	 * QuasarOperationChallengeAPIException
	 * 
	 * @param code
	 * @param message
	 * @param details
	 * @param date
	 * @param cause
	 */
	public QuasarOperationChallengeAPIException(Integer code, String message, String details, LocalDateTime date,
			Throwable cause) {
		super(cause);
		this.responseCode = code;
		this.responseMessage = message;
		this.details = details;
		this.date = date.format(DF);
	}

	/**
	 * @return the responseCode
	 */
	public Integer getResponseCode() {
		return responseCode;
	}

	/**
	 * @param responseCode the responseCode to set
	 */
	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}

	/**
	 * @return the responseMessage
	 */
	public String getResponseMessage() {
		return responseMessage;
	}

	/**
	 * @param responseMessage the responseMessage to set
	 */
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	/**
	 * @return the details
	 */
	public String getDetails() {
		return details;
	}

	/**
	 * @param details the details to set
	 */
	public void setDetails(String details) {
		this.details = details;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

}

package com.quasar.operation.challenge.api.controller;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.quasar.operation.challenge.api.exception.QuasarOperationChallengeAPIException;
import com.quasar.operation.challenge.api.properties.QuasarOperationChallengeAPIProperties;
import com.quasar.operation.challenge.api.utils.Utils;

@ControllerAdvice
@RestController
public class QuasarOperationChallengeAPIExceptionHandlerController extends ResponseEntityExceptionHandler {

	private static final Logger LOGGER_EXHDL = LoggerFactory
			.getLogger(QuasarOperationChallengeAPIExceptionHandlerController.class);

	private String details = "";

	@Autowired
	QuasarOperationChallengeAPIProperties properties;

	/**
	 * Handle Method All Exceptions
	 * 
	 * @param ex
	 * @param request
	 * @return
	 */
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<QuasarOperationChallengeAPIException> handleMethodAllExceptions(Exception ex,
			WebRequest request) {
		LOGGER_EXHDL.trace(Utils.INSIDE_METHOD, Utils.METHOD_HANDLE_ALL_EXCEPTIONS);

		LOGGER_EXHDL.debug(Utils.LOG_REQUEST, request);
		LOGGER_EXHDL.debug(Utils.LOG_REQUEST_PARAMS, request.getContextPath(), request.getHeaderNames(),
				request.getParameterMap());
		LOGGER_EXHDL.error(Utils.LOG_ERROR, ex);

		QuasarOperationChallengeAPIException err = new QuasarOperationChallengeAPIException(properties.getErrorCode(),
				properties.getExceptionMsg(), request.getDescription(false), LocalDateTime.now(), ex);

		LOGGER_EXHDL.error(Utils.LOG_RESP_QUASAR_OPER_CHALLENGE_API_EXCEPTION, err.getResponseCode(), err.getMessage(),
				ex, err.getDate());

		LOGGER_EXHDL.trace(Utils.FINISH_METHOD, Utils.METHOD_HANDLE_ALL_EXCEPTIONS);
		return new ResponseEntity<>(err, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	/**
	 * Handle Method Argument Not Valid
	 * 
	 * @param ex
	 * @param headers
	 * @param status
	 * @param request
	 * @return
	 */
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		LOGGER_EXHDL.trace(Utils.INSIDE_METHOD, Utils.METHOD_HANDLE_ARGUMENT_NOT_VALID);

		ex.getBindingResult().getAllErrors().forEach(e -> {
			details += e.getDefaultMessage() + "\n";
		});

		QuasarOperationChallengeAPIException err = new QuasarOperationChallengeAPIException(properties.getErrorCode(),
				properties.getExceptionMsg(), details, LocalDateTime.now());

		LOGGER_EXHDL.error(Utils.LOG_RESP_QUASAR_OPER_CHALLENGE_API_EXCEPTION, err.getResponseCode(), err.getMessage(),
				ex, err.getDate());

		LOGGER_EXHDL.trace(Utils.FINISH_METHOD, Utils.METHOD_HANDLE_ARGUMENT_NOT_VALID);
		return new ResponseEntity<>(err, HttpStatus.BAD_REQUEST);
	}

	/**
	 * Handle HttpRequest Method Not Supported
	 * 
	 * @param ex
	 * @param headers
	 * @param status
	 * @param request
	 * @return
	 */
	@Override
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		LOGGER_EXHDL.trace(Utils.INSIDE_METHOD, Utils.METHOD_HANDLE_HTTP_REQUEST_METHOD_NOT_SUPPORTED);

		QuasarOperationChallengeAPIException err = new QuasarOperationChallengeAPIException(properties.getErrorCode(),
				properties.getExceptionMsg(), details, LocalDateTime.now());

		LOGGER_EXHDL.error(Utils.LOG_RESP_QUASAR_OPER_CHALLENGE_API_EXCEPTION, err.getResponseCode(), err.getMessage(),
				ex, err.getDate());

		LOGGER_EXHDL.trace(Utils.FINISH_METHOD, Utils.METHOD_HANDLE_HTTP_REQUEST_METHOD_NOT_SUPPORTED);
		return new ResponseEntity<>(err, HttpStatus.METHOD_NOT_ALLOWED);
	}
}

package com.quasar.operation.challenge.api.handler.chain;

import com.quasar.operation.challenge.api.dto.lib.ChallengeAPIResponse;
import com.quasar.operation.challenge.api.dto.lib.Satellite;
import com.quasar.operation.challenge.api.properties.QuasarOperationChallengeAPIProperties;
import com.quasar.operation.challenge.api.utils.Utils;

public class SatelliteNumberHandler extends AbstractFilterArraySatelliteHandler {

	@Override
	public ChallengeAPIResponse performValidation(Satellite[] satellites,
			QuasarOperationChallengeAPIProperties properties) {

		if (!properties.getNumberSatellites().equals(satellites.length)) {
			LOGGER.warn(Utils.LOG_BAD_REQUEST_INVALID_NUMBER_OF_SATELLITES);
			return Utils.buildingResponse(properties.getErrorCode(), properties.getBadInvalidNumberSatellitesMsg());
			
		} else {
			return nextValidation.performValidation(satellites, properties);
		}
	}

}

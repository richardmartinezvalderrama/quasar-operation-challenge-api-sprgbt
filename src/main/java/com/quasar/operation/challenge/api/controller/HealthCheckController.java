package com.quasar.operation.challenge.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.quasar.operation.challenge.api.utils.Utils;

@RestController
@RequestMapping(path = "/")
public class HealthCheckController {

	private static final Logger LOGGER = LoggerFactory.getLogger(HealthCheckController.class);

	@GetMapping("/healthCheck")
	public ResponseEntity<Object> healthCheck() {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_HEALTH_CHECK);

		LOGGER.info("path: /healtcheck");

		String responseStr = "healtcheck";

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_HEALTH_CHECK);
		return new ResponseEntity<Object>(responseStr, HttpStatus.OK);
	}

}

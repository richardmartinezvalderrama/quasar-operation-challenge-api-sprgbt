package com.quasar.operation.challenge.api.service;

import com.quasar.operation.challenge.api.dto.lib.ChallengeAPIResponse;
import com.quasar.operation.challenge.api.dto.lib.Satellite;
import com.quasar.operation.challenge.api.dto.lib.TopSecretChallengeAPIRequest;

public interface IQuasarOperationChallengeAPI {

	/**
	 * TopSecret
	 * 
	 * Template of the service contract that defines the behavior to obtain the
	 * location and the message emitted by the spaceship
	 * 
	 * @param TopSecretChallengeAPIRequest: It contains an array with the name, the
	 *                                      distance and the message received in
	 *                                      each of the satellites
	 * 
	 * @return ChallengeAPIResponse: Contains the position of the spaceship and the
	 *         message issued by it
	 */
	public ChallengeAPIResponse topSecret(TopSecretChallengeAPIRequest request);

	/**
	 * TopSecret_Split
	 * 
	 * EndPoint in charge of receiving in different POST the information associated
	 * with each satellite
	 * 
	 * @param request:       associated with the satellite (distance and message)
	 * @param satelliteName: associated with the satellite name
	 * 
	 * @return ChallengeAPIResponse: Returns a code and a response message
	 *         indicating that the satellite information was stored correctly
	 */
	public ChallengeAPIResponse topSecretSplit(Satellite request, String satelliteName);

	/**
	 * GetTopSecret_Split
	 * 
	 * EndPoint in charge of obtaining the location and the message that the
	 * spaceship emits
	 * 
	 * @return ChallengeAPIResponse: Contains the position of the spaceship and the
	 *         message issued by it
	 */
	public ChallengeAPIResponse getTopSecretSplit();

}

package com.quasar.operation.challenge.api.handler.chain;

import org.apache.commons.lang3.ArrayUtils;

import com.quasar.operation.challenge.api.dto.lib.ChallengeAPIResponse;
import com.quasar.operation.challenge.api.dto.lib.Satellite;
import com.quasar.operation.challenge.api.properties.QuasarOperationChallengeAPIProperties;
import com.quasar.operation.challenge.api.utils.Utils;

public class MessageArrayHandler extends AbstractFilterArraySatelliteHandler {

	@Override
	public ChallengeAPIResponse performValidation(Satellite[] satellites,
			QuasarOperationChallengeAPIProperties properties) {
		
		if (ArrayUtils.isEmpty(satellites[0].getMessage()) || ArrayUtils.isEmpty(satellites[1].getMessage())
				|| ArrayUtils.isEmpty(satellites[2].getMessage())) {
			LOGGER.warn(Utils.LOG_BAD_REQUEST_BAD_SATELLITE_MESSAGE);
			return Utils.buildingResponse(properties.getErrorCode(), properties.getBadSatelliteMessageMsg());

		} else {
			return nextValidation.performValidation(satellites, properties);
		}
	}

}

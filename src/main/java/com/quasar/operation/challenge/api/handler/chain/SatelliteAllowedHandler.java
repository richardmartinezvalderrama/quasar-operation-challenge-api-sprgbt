package com.quasar.operation.challenge.api.handler.chain;

import org.apache.commons.lang3.ArrayUtils;

import com.quasar.operation.challenge.api.dto.lib.ChallengeAPIResponse;
import com.quasar.operation.challenge.api.dto.lib.Satellite;
import com.quasar.operation.challenge.api.properties.QuasarOperationChallengeAPIProperties;
import com.quasar.operation.challenge.api.utils.Utils;

public class SatelliteAllowedHandler extends AbstractFilterSatelliteHandler {

	@Override
	public ChallengeAPIResponse performValidation(Satellite satellite,
			QuasarOperationChallengeAPIProperties properties) {
		if (!checkSatelliteName(satellite.getName(), properties)) {
			LOGGER.warn(Utils.LOG_BAD_REQUEST_BAD_SATELLITE_NAME);
			return Utils.buildingResponse(properties.getErrorCode(), properties.getBadSatelliteNameMsg());

		} else {
			return nextValidation.performValidation(satellite, properties);
		}
	}

	/**
	 * Method in charge of validating if the name of the satellite received is
	 * within the property corresponding to the list of names of the satellites
	 * located
	 * 
	 * @param nameSatellite
	 * @return true if the name is within the list of names of the satellites
	 *         located or false otherwise
	 */
	private boolean checkSatelliteName(String nameSatellite, QuasarOperationChallengeAPIProperties properties) {
		LOGGER.trace(Utils.INSIDE_METHOD, Utils.METHOD_CHECK_SATELLITE_NAME);

		boolean dtoResp = false;

		String nameSatellites[] = properties.getNameSatellites().split(properties.getSplitRegex());

		if (!ArrayUtils.isEmpty(nameSatellites)) {

			for (String name : nameSatellites) {
				if (name.equals(nameSatellite.toUpperCase())) {
					dtoResp = true;
				}
			}
		}

		LOGGER.trace(Utils.FINISH_METHOD, Utils.METHOD_CHECK_SATELLITE_NAME);
		return dtoResp;
	}
}

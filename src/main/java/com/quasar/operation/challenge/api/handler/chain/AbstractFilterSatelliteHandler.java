package com.quasar.operation.challenge.api.handler.chain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quasar.operation.challenge.api.dto.lib.ChallengeAPIResponse;
import com.quasar.operation.challenge.api.dto.lib.Satellite;
import com.quasar.operation.challenge.api.properties.QuasarOperationChallengeAPIProperties;

public abstract class AbstractFilterSatelliteHandler {

	protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractFilterSatelliteHandler.class);

	protected AbstractFilterSatelliteHandler nextValidation;

	/*
	 * Setter
	 */

	/**
	 * Builds chains of AbstractFilterSatelliteHandler objects.
	 * 
	 * @param nextValidation the nextValidation to set
	 */
	public void setNextValidation(AbstractFilterSatelliteHandler nextValidation) {
		this.nextValidation = nextValidation;
	}

	/**
	 * Handling method in charge of performing the validations
	 * 
	 * @param satellites
	 * @param properties
	 * @return
	 */
	public abstract ChallengeAPIResponse performValidation(Satellite satellite,
			QuasarOperationChallengeAPIProperties properties);

}

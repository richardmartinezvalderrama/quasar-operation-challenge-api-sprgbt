package com.quasar.operation.challenge.api.handler.chain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quasar.operation.challenge.api.dto.lib.ChallengeAPIResponse;
import com.quasar.operation.challenge.api.dto.lib.Satellite;
import com.quasar.operation.challenge.api.properties.QuasarOperationChallengeAPIProperties;

public abstract class AbstractFilterArraySatelliteHandler {

	protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractFilterArraySatelliteHandler.class);

	protected AbstractFilterArraySatelliteHandler nextValidation;

	/*
	 * Setter
	 */

	/**
	 * Builds chains of AbstractFilterHandler objects.
	 * 
	 * @param nextValidation the nextValidation to set
	 */
	public void setNextValidation(AbstractFilterArraySatelliteHandler nextValidation) {
		this.nextValidation = nextValidation;
	}

	/**
	 * Handling method in charge of performing the validations
	 * @param satellites
	 * @param properties
	 * @return
	 */
	public abstract ChallengeAPIResponse performValidation(Satellite[] satellites,
			QuasarOperationChallengeAPIProperties properties);

}

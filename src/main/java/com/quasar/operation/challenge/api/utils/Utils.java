package com.quasar.operation.challenge.api.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quasar.operation.challenge.api.dto.lib.ChallengeAPIResponse;
import com.quasar.operation.challenge.api.dto.lib.Position;

public class Utils {

	private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);

	private static final double EPSILON = 0.01;

	public static final String INSIDE_METHOD = "Inside method: {}";
	public static final String FINISH_METHOD = "Finish method: {}";
	public static final String LOG_EXCEPTION = "EXCEPTION: ";

	public static final String KENOBI = "KENOBI";
	public static final String SKYWALKER = "SKYWALKER";
	public static final String SATO = "SATO";

	public static final String METHOD_HEALTH_CHECK = "HEALTH_CHECK";
	public static final String METHOD_HANDLE_ALL_EXCEPTIONS = "HANDLE_ALL_EXCEPTIONS";
	public static final String METHOD_HANDLE_ARGUMENT_NOT_VALID = "HANDLE_ARGUMENT_NOT_VALID";
	public static final String METHOD_HANDLE_HTTP_REQUEST_METHOD_NOT_SUPPORTED = "_HANDLE_HTTP_REQUEST_METHOD_NOT_SUPPORTED";

	public static final String METHOD_TOP_SECRET = "TOP_SECRET";
	public static final String METHOD_PREVIOUS_VALIDATIONS_TOP_SECRET_REQUEST = "PREVIOUS_VALIDATIONS_TOP_SECRET_REQUEST";
	public static final String METHOD_CHECK_SATELLITE_INFO = "CHECK_SATELLITE_INFO";
	public static final String METHOD_CHECK_SATELLITE_NAME = "CHECK_SATELLITE_NAME";
	public static final String METHOD_PROCESS_TOP_SECRET = "PROCESS_TOP_SECRET";
	public static final String METHOD_GET_LOCATION = "GET_LOCATION";
	public static final String METHOD_GET_MESSAGE = "GET_MESSAGE";
	public static final String METHOD_CALCULATE_POINT_INTERSECTION_BETWEEN_SATELLITES = "CALCULATE_POINT_INTERSECTION_BETWEEN_SATELLITES";
	public static final String METHOD_BUILDING_RESPONSE = "BUILDING_RESPONSE";

	public static final String METHOD_TOP_SECRET_SPLIT = "TOP_SECRET_SPLIT";
	public static final String METHOD_GET_TOP_SECRET_SPLIT = "GET_TOP_SECRET_SPLIT";

	public static final String LOG_BUILDING_RESPONSE_METHOD = "BUILDING_RESPONSE_METHOD: {}";
	public static final String LOG_TOP_SECRET_METHOD = "TOP_SECRET_RESPONSE_METHOD: [ responseCode: {} - responseMessage: {} ]";
	public static final String LOG_TOP_SECRET_SPLIT_METHOD = "TOP_SECRET_SPLIT_RESPONSE_METHOD: [ responseCode: {} - responseMessage: {} ]";
	public static final String LOG_GET_TOP_SECRET_SPLIT_METHOD = "GET_TOP_SECRET_SPLIT_RESPONSE_METHOD: [ responseCode: {} - responseMessage: {} ]";
	public static final String LOG_REQUEST = "Request: [ {} ]";
	public static final String LOG_REQUEST_PARAMS = "Request: [ ContextPath: {} - HeaderNames: {} - ParameterMap: {} ]";
	public static final String LOG_ERROR = "ERROR : ";
	public static final String LOG_RESP_QUASAR_OPER_CHALLENGE_API_EXCEPTION = "QUASAR_OPER_CHALLENGE_API_EXCEPTION: [ code: {} - message: {} - details: {} - date: {} ]";
	public static final String LOG_BAD_REQUEST_ARRAY_SATELLITES_IS_EMPTY = "BAD_REQUEST_ARRAY_SATELLITES_IS_EMPTY";
	public static final String LOG_BAD_REQUEST_INVALID_NUMBER_OF_SATELLITES = "BAD_REQUEST_INVALID_NUMBER_OF_SATELLITES";
	public static final String LOG_BAD_REQUEST_LENGTH_MESSAGE_RECEIVED_ON_EACH_SATELLITE_IS_DIFFERENT = "BAD_REQUEST_LENGTH_MESSAGE_RECEIVED_ON_EACH_SATELLITE_IS_DIFFERENT";
	public static final String LOG_BAD_REQUEST_NULL_SATELLITE_NAME = "NULL_SATELLITE_NAME";
	public static final String LOG_BAD_REQUEST_BAD_SATELLITE_NAME = "BAD_SATELLITE_NAME";
	public static final String LOG_BAD_REQUEST_BAD_SATELLITE_DISTANCE = "BAD_SATELLITE_DISTANCE";
	public static final String LOG_BAD_REQUEST_BAD_SATELLITE_MESSAGE = "AD_ARRAY_SATELLITE_MESSAGE";
	public static final String LOG_ERROR_METHOD = "ERROR_METHOD: [ {} ] - [ responseCode: {} - responseMessage: {} ]";
	public static final String LOG_PROCESS_TOP_SECRET = "Proceed to obtain the location and the message issued by the spaceship...";
	public static final String LOG_GET_LOCATION_AND_MESSAGE_FROM_SPACESHIP = "Get location and message from spaceship...";
	public static final String LOG_VALIDATION_FIELDS_RECEIVED_IN_REQUEST_BODY = "validation of the fields received in the request body...";
	public static final String LOG_CHECK_PERFORM_VALIDATION_REQUEST_BODY = "Check perform validation request body: [ {} ]";
	public static final String LOG_SATELLITES_ARE_ORDERED = "Satellites are ordered by alphabetical name to proceed to operate them...";
	public static final String LOG_GET_POSITION_SPACESHIP = "Get position of the spaceship...";
	public static final String LOG_GET_MESSAGE_SPACESHIP = "Get message issued by the the spaceship...";
	public static final String LOG_BUILD_SATELLITE_OBJECT = "Build Satellite object...";
	public static final String LOG_NOT_ENOUGH_INFO_ABOUT_SATELLITES = "Not enough information about satellites...";
	public static final String LOG_DEFINE_CONCRETE_HANDLERS = "Define the concrete handlers...";
	public static final String LOG_DEFINE_CHAIN_OF_RESPONSIBILITY_HANDLERS = "Define the chain of responsibility for handlers...";
	public static final String LOG_START_SPECIFIC_HANDLER_IN_CHARGE_OF_PERFORM_VALIDATIONS = "Start the specific handler in charge of perform the validations...";
	public static final String LOG_CALCULATE_POINT_INTERSECTION_BETWEEN_SATELLITES = "Calculate point intersection between Satellites...";

	/**
	 * Default Constructor
	 */
	private Utils() {
		throw new IllegalStateException("Utils class...");
	}

	/**
	 * Method that calculates the point of intersection between the satellites and
	 * the spaceship
	 * 
	 * Solution: Using this method, find the intersection of any two circles... lets
	 * say (x,y). Now the third circle will intersect at point (x,y) only if
	 * distance between its center and point x,y is equal to r.
	 * 
	 * case 1) If distance(center,point) == r, then x,y is the intersection point.
	 * 
	 * case 2) If distance(center,point) != r, then no such point exists.
	 * 
	 * Also, define EPSILON to a small value that is acceptable for your application
	 * requirements
	 * 
	 * 
	 * @param x0: Satellite coordinate X
	 * @param y0: Satellite coordinate Y
	 * @param r0: Distance from satellite to spaceship
	 * 
	 * @param x1: Satellite coordinate X
	 * @param y1: Satellite coordinate Y
	 * @param r1: Distance from satellite to spaceship
	 * 
	 * @param x2: Satellite coordinate X
	 * @param y2: Satellite coordinate Y
	 * @param r2: Distance from satellite to spaceship
	 * 
	 * @return Position: position corresponding to the location of the spaceship
	 */
	public static Position calculatePointIntersectionBetweenSatellites(double x0, double y0, double r0, double x1,
			double y1, double r1, double x2, double y2, double r2) {
		LOGGER.trace(INSIDE_METHOD, METHOD_CALCULATE_POINT_INTERSECTION_BETWEEN_SATELLITES);

		Position position = null;

		double a, dx, dy, d, h, rx, ry;
		double point2_x, point2_y;

		/*
		 * dx and dy are the vertical and horizontal distances between the circle
		 * centers.
		 */
		dx = x2 - x0;
		dy = y2 - y0;

		/* Determine the straight-line distance between the centers. */
		d = Math.sqrt((dy * dy) + (dx * dx));

		/* Check for solvability. */
		if (d > (r0 + r2)) {
			/* no solution. satellites do not intersect. */
			return position;
		}
		if (d < Math.abs(r0 - r2)) {
			/* no solution. one satellites is contained in the other */
			return position;
		}

		/*
		 * 'point 2' is the point where the line through the circle intersection points
		 * crosses the line between the circle centers.
		 */

		/* Determine the distance from point 0 to point 2. */
		a = ((r0 * r0) - (r2 * r2) + (d * d)) / (2.0 * d);

		/* Determine the coordinates of point 2. */
		point2_x = x0 + (dx * a / d);
		point2_y = y0 + (dy * a / d);

		/*
		 * Determine the distance from point 2 to either of the intersection points.
		 */
		h = Math.sqrt((r0 * r0) - (a * a));

		/*
		 * Now determine the offsets of the intersection points from point 2.
		 */
		rx = -dy * (h / d);
		ry = dx * (h / d);

		/* Determine the absolute intersection points. */
		double intersectionPoint1_x = point2_x + rx;
		double intersectionPoint2_x = point2_x - rx;
		double intersectionPoint1_y = point2_y + ry;
		double intersectionPoint2_y = point2_y - ry;

		LOGGER.debug("INTERSECTION between Satellite1 AND Satellite2: point1 ({},{})  AND  point2 ({},{})",
				intersectionPoint1_x, intersectionPoint1_y, intersectionPoint2_x, intersectionPoint2_y);

		/*
		 * Lets determine if satellite 3 intersects at either of the above intersection
		 * points.
		 */
		dx = intersectionPoint1_x - x1;
		dy = intersectionPoint1_y - y1;
		double d1 = Math.sqrt((dy * dy) + (dx * dx));

		dx = intersectionPoint2_x - x1;
		dy = intersectionPoint2_y - y1;
		double d2 = Math.sqrt((dy * dy) + (dx * dx));

		if (Math.abs(d1 - r1) < EPSILON) {
			LOGGER.info("INTERSECTION between Satellite1 AND Satellite2 AND Satellite3: point1 ({},{})",
					intersectionPoint1_x, intersectionPoint1_y);

			position = new Position(
					(float) Double.parseDouble(String.valueOf(Math.round(intersectionPoint1_x * 100.0) / 100.0)),
					(float) Double.parseDouble(String.valueOf(Math.round(intersectionPoint1_y * 100.0) / 100.0)));

		} else if (Math.abs(d2 - r1) < EPSILON) {
			LOGGER.info("INTERSECTION between Satellite1 AND Satellite2 AND Satellite3: point2 ({},{})",
					intersectionPoint2_x, intersectionPoint2_y);

			position = new Position(
					(float) Double.parseDouble(String.valueOf(Math.round(intersectionPoint2_x * 100.0) / 100.0)),
					(float) Double.parseDouble(String.valueOf(Math.round(intersectionPoint2_y * 100.0) / 100.0)));

		} else {
			LOGGER.warn("INTERSECTION between Satellite1 AND Satellite2 AND Satellite3 was NOT FOUND");
		}

		LOGGER.trace(FINISH_METHOD, METHOD_CALCULATE_POINT_INTERSECTION_BETWEEN_SATELLITES);
		return position;
	}

	/**
	 * Used to construct the response
	 * 
	 * @param responseCode
	 * @param responseMessage
	 * @return ChallengeAPIResponse
	 */
	public static ChallengeAPIResponse buildingResponse(Integer responseCode, String responseMessage) {
		LOGGER.trace(INSIDE_METHOD, METHOD_BUILDING_RESPONSE);

		ChallengeAPIResponse dtoResp = new ChallengeAPIResponse(responseCode, responseMessage);

		LOGGER.trace(FINISH_METHOD, METHOD_BUILDING_RESPONSE);
		return dtoResp;
	}

}

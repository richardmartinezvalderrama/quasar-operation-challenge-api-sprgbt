package com.quasar.operation.challenge.api.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ExternalComponentsConfiguration {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExternalComponentsConfiguration.class);

	/**
	 * Configuration from business properties.
	 */

	@Configuration
	@PropertySource(value = "file:${business.properties.file.name}", encoding = "UTF-8")
	static class BasicBusinessProperties {
	}

	@Bean
	public Gson gson() {
		return new GsonBuilder().setPrettyPrinting().create();
	}
}

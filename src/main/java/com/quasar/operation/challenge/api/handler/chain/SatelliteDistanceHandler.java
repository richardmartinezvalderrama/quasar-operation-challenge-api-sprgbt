package com.quasar.operation.challenge.api.handler.chain;

import com.quasar.operation.challenge.api.dto.lib.ChallengeAPIResponse;
import com.quasar.operation.challenge.api.dto.lib.Satellite;
import com.quasar.operation.challenge.api.properties.QuasarOperationChallengeAPIProperties;
import com.quasar.operation.challenge.api.utils.Utils;

public class SatelliteDistanceHandler extends AbstractFilterSatelliteHandler {

	@Override
	public ChallengeAPIResponse performValidation(Satellite satellite,
			QuasarOperationChallengeAPIProperties properties) {

		if (satellite.getDistance() <= 0.0f) {
			LOGGER.warn(Utils.LOG_BAD_REQUEST_BAD_SATELLITE_DISTANCE);
			return Utils.buildingResponse(properties.getErrorCode(), properties.getBadSatelliteDistanceMsg());

		} else {
			return nextValidation.performValidation(satellite, properties);
		}
	}

}

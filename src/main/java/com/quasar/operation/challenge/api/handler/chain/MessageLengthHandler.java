package com.quasar.operation.challenge.api.handler.chain;

import com.quasar.operation.challenge.api.dto.lib.ChallengeAPIResponse;
import com.quasar.operation.challenge.api.dto.lib.Satellite;
import com.quasar.operation.challenge.api.properties.QuasarOperationChallengeAPIProperties;
import com.quasar.operation.challenge.api.utils.Utils;

public class MessageLengthHandler extends AbstractFilterArraySatelliteHandler {

	@Override
	public ChallengeAPIResponse performValidation(Satellite[] satellites,
			QuasarOperationChallengeAPIProperties properties) {
		
		if (Boolean.FALSE.equals(satellites[0].getMessage().length == satellites[1].getMessage().length
				&& satellites[1].getMessage().length == satellites[2].getMessage().length)) {
			LOGGER.warn(Utils.LOG_BAD_REQUEST_LENGTH_MESSAGE_RECEIVED_ON_EACH_SATELLITE_IS_DIFFERENT);
			return Utils.buildingResponse(properties.getErrorCode(), properties.getErrorLengthDifferentMsg());

		} else {
			return nextValidation.performValidation(satellites, properties);
		}
	}

}
